﻿#pragma checksum "..\..\MainWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "801CAB4486C63840D46B284FB7C5F8C2A91DABF8502D1067D7855F80CCB9B518"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using galgje;


namespace galgje {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridBg;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnRaad;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnHint;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNieuwSpel;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnVerbergWoord;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn1Speler;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnScore;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblOpmerking;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblInfo;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblcountdown;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtingaveCountdown;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblcountdownvraag;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtIngeefveld;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHangman0;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHangman1;
        
        #line default
        #line hidden
        
        
        #line 153 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHangman2;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHangman3;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHangman4;
        
        #line default
        #line hidden
        
        
        #line 174 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHangman5;
        
        #line default
        #line hidden
        
        
        #line 181 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHangman6;
        
        #line default
        #line hidden
        
        
        #line 188 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHangman7;
        
        #line default
        #line hidden
        
        
        #line 195 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHangman8;
        
        #line default
        #line hidden
        
        
        #line 202 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHangman9;
        
        #line default
        #line hidden
        
        
        #line 209 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHangman10;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTimer;
        
        #line default
        #line hidden
        
        
        #line 220 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtScoreBord;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/galgje;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.gridBg = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.btnRaad = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\MainWindow.xaml"
            this.btnRaad.Click += new System.Windows.RoutedEventHandler(this.btnRaad_Click);
            
            #line default
            #line hidden
            
            #line 37 "..\..\MainWindow.xaml"
            this.btnRaad.MouseEnter += new System.Windows.Input.MouseEventHandler(this.btnRaad_MouseEnter);
            
            #line default
            #line hidden
            
            #line 38 "..\..\MainWindow.xaml"
            this.btnRaad.MouseLeave += new System.Windows.Input.MouseEventHandler(this.btnRaad_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnHint = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\MainWindow.xaml"
            this.btnHint.Click += new System.Windows.RoutedEventHandler(this.btnHint_Click);
            
            #line default
            #line hidden
            
            #line 50 "..\..\MainWindow.xaml"
            this.btnHint.MouseEnter += new System.Windows.Input.MouseEventHandler(this.btnHint_MouseEnter);
            
            #line default
            #line hidden
            
            #line 50 "..\..\MainWindow.xaml"
            this.btnHint.MouseLeave += new System.Windows.Input.MouseEventHandler(this.btnHint_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnNieuwSpel = ((System.Windows.Controls.Button)(target));
            
            #line 60 "..\..\MainWindow.xaml"
            this.btnNieuwSpel.Click += new System.Windows.RoutedEventHandler(this.btnNieuwSpel_Click);
            
            #line default
            #line hidden
            
            #line 63 "..\..\MainWindow.xaml"
            this.btnNieuwSpel.MouseEnter += new System.Windows.Input.MouseEventHandler(this.btnNieuwSpel_MouseEnter);
            
            #line default
            #line hidden
            
            #line 63 "..\..\MainWindow.xaml"
            this.btnNieuwSpel.MouseLeave += new System.Windows.Input.MouseEventHandler(this.btnNieuwSpel_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnVerbergWoord = ((System.Windows.Controls.Button)(target));
            
            #line 73 "..\..\MainWindow.xaml"
            this.btnVerbergWoord.Click += new System.Windows.RoutedEventHandler(this.btnVerbergWoord_Click);
            
            #line default
            #line hidden
            
            #line 76 "..\..\MainWindow.xaml"
            this.btnVerbergWoord.MouseEnter += new System.Windows.Input.MouseEventHandler(this.btnVerbergWoord_MouseEnter);
            
            #line default
            #line hidden
            
            #line 76 "..\..\MainWindow.xaml"
            this.btnVerbergWoord.MouseLeave += new System.Windows.Input.MouseEventHandler(this.btnVerbergWoord_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btn1Speler = ((System.Windows.Controls.Button)(target));
            
            #line 83 "..\..\MainWindow.xaml"
            this.btn1Speler.Click += new System.Windows.RoutedEventHandler(this.btn1Speler_Click);
            
            #line default
            #line hidden
            
            #line 86 "..\..\MainWindow.xaml"
            this.btn1Speler.MouseEnter += new System.Windows.Input.MouseEventHandler(this.btn1Speler_MouseEnter);
            
            #line default
            #line hidden
            
            #line 86 "..\..\MainWindow.xaml"
            this.btn1Speler.MouseLeave += new System.Windows.Input.MouseEventHandler(this.btn1Speler_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnScore = ((System.Windows.Controls.Button)(target));
            
            #line 93 "..\..\MainWindow.xaml"
            this.btnScore.Click += new System.Windows.RoutedEventHandler(this.btnScore_Click);
            
            #line default
            #line hidden
            
            #line 96 "..\..\MainWindow.xaml"
            this.btnScore.MouseEnter += new System.Windows.Input.MouseEventHandler(this.btnScore_MouseEnter);
            
            #line default
            #line hidden
            
            #line 96 "..\..\MainWindow.xaml"
            this.btnScore.MouseLeave += new System.Windows.Input.MouseEventHandler(this.btnScore_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 8:
            this.lblOpmerking = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.lblInfo = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.lblcountdown = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.txtingaveCountdown = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.lblcountdownvraag = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.txtIngeefveld = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.imgHangman0 = ((System.Windows.Controls.Image)(target));
            return;
            case 15:
            this.imgHangman1 = ((System.Windows.Controls.Image)(target));
            return;
            case 16:
            this.imgHangman2 = ((System.Windows.Controls.Image)(target));
            return;
            case 17:
            this.imgHangman3 = ((System.Windows.Controls.Image)(target));
            return;
            case 18:
            this.imgHangman4 = ((System.Windows.Controls.Image)(target));
            return;
            case 19:
            this.imgHangman5 = ((System.Windows.Controls.Image)(target));
            return;
            case 20:
            this.imgHangman6 = ((System.Windows.Controls.Image)(target));
            return;
            case 21:
            this.imgHangman7 = ((System.Windows.Controls.Image)(target));
            return;
            case 22:
            this.imgHangman8 = ((System.Windows.Controls.Image)(target));
            return;
            case 23:
            this.imgHangman9 = ((System.Windows.Controls.Image)(target));
            return;
            case 24:
            this.imgHangman10 = ((System.Windows.Controls.Image)(target));
            return;
            case 25:
            this.lblTimer = ((System.Windows.Controls.Label)(target));
            return;
            case 26:
            this.txtScoreBord = ((System.Windows.Controls.TextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

