﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.VisualBasic;

namespace galgje
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //galgje woorden deel
        string geheimWoord;
        string verborgenGeheimWoord;        //gemaskte woord
        string geraden;                     //ingegeven letter of woord
        string juistenLetters;
        string fouteLetters;
        string controleerletterwoord;       //zijn alle letters geraden

        char[] verborgenLetters;            

        bool letterJuist;                   //letter juist ? 

        //levens
        int levens = 10;

        //algemene timer
        DispatcherTimer teller; 
        DateTime timer = new DateTime();


        //countdown
        DispatcherTimer countDownteller;       
        int countdownnumer;
        int ingegevencountdownNummer;
        DispatcherTimer tussentimer;
        int tussentimernummer = 0;
            
        bool countDownIngavenNummer;        //is het een nummer?



        //scorebord
        List<string> spelersNamen = new List<string>();
        string[] spelersNamenArray;
        List<string> spelerTijden = new List<string>();
        string[] spelersTijdenArray;
        List<int> spelerLeven = new List<int>();
        int[] spelersLevenArray;
        string spelerNaam;
        int idx = 0;
        bool scoreOpen = false;

        //hint
        string[] alfabet = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
        string hintletter;
        bool hintGebruikt = false;
        int maxHint = 5;
        


        public MainWindow()
        {
           
            InitializeComponent();

            btnRaad.Visibility = Visibility.Hidden;
            lblcountdown.Visibility = Visibility.Hidden;
            btnHint.Visibility = Visibility.Hidden;
            //btnScoreWeg.Visibility = Visibility.Hidden;
            btnScore.IsEnabled = false;

            

            //algemene speltimer
            teller = new DispatcherTimer();
            teller.Tick += Timer_Tick;
            teller.Interval = new TimeSpan(0, 0, 1);
            lblTimer.Content = timer.ToLongTimeString();
            

            //countdown beuten timer
            countDownteller = new DispatcherTimer();
            countDownteller.Tick += CountDown_Tick;
            countDownteller.Interval = new TimeSpan(0, 0, 1);

            tussentimer = new DispatcherTimer();
            tussentimer.Tick += Tussentimer_Tick;
            tussentimer.Interval = new TimeSpan(0, 0, 1);      
                     
           
        }

        //timers--------------------------------------------
        private void Tussentimer_Tick(object sender, EventArgs e)
        {

            if(tussentimernummer<1)
            {                
                
                tussentimernummer++;
            }
            else
            {
                tussentimer.Stop();
                tussentimernummer = 0;
                var bc = new BrushConverter();
                gridBg.Background = (Brush)bc.ConvertFrom("#FFABC199");
                countDownteller.Start();
                btnRaad.IsEnabled = true;


            }
        }

        private void CountDown_Tick(object sender, EventArgs e)
        {
            lblcountdown.Content = countdownnumer;

            if (countdownnumer > 0)
            {
                countdownnumer--;
                
            }
            else
            {
                countDownteller.Stop();
                
                gridBg.Background = Brushes.Red;

                levens--;
                AfbeeldingTonen();

                lblOpmerking.Content = "Te laat geantwoord";
                TextInfo();

                countdownnumer = ingegevencountdownNummer;

                tussentimer.Start();

                btnRaad.IsEnabled = false;
                
            }

            if (levens == 0)
            {
                GameOver();
                countDownteller.Stop();
            }
               

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            //timer.AddSeconds(1);
            timer = timer + teller.Interval;
            

            lblTimer.Content = timer.ToLongTimeString();
                       

        }



        //buttons---------------------------------------------
        private void btnNieuwSpel_Click(object sender, RoutedEventArgs e)
        {
            //begin requarments
            btnVerbergWoord.IsEnabled = true;
            txtIngeefveld.IsEnabled = true;
            lblInfo.Content = $"Geef een geheim woord\nof klik op 1 Speler";

            //reset requarments
            btnRaad.Visibility = Visibility.Hidden;
            geheimWoord = "";
            btnVerbergWoord.Visibility = Visibility.Visible;
            levens = 10;
            btn1Speler.Visibility = Visibility.Visible;
            juistenLetters = "";
            fouteLetters = "";
            verborgenGeheimWoord = "";
            lblOpmerking.Content = "";
            lblcountdown.Visibility = Visibility.Hidden;
            lblcountdownvraag.Visibility = Visibility.Visible;
            txtingaveCountdown.Visibility = Visibility.Visible;
            btnScore.Visibility = Visibility.Visible;
           
            
            //hint resetten
            hintGebruikt = false; 
            btnHint.Visibility = Visibility.Hidden;
            maxHint = 5;

            //timer stoppen en resetten 
            teller.Stop();
            countDownteller.Stop();
            countdownnumer = 10;
            timer = new DateTime();
            lblTimer.Content = timer.ToLongTimeString();

            

            //afbeeldingen resetten
            imgHangman0.Visibility = Visibility.Visible;
            imgHangman1.Visibility = Visibility.Hidden;
            imgHangman2.Visibility = Visibility.Hidden;
            imgHangman3.Visibility = Visibility.Hidden;
            imgHangman4.Visibility = Visibility.Hidden;
            imgHangman5.Visibility = Visibility.Hidden;
            imgHangman6.Visibility = Visibility.Hidden;
            imgHangman7.Visibility = Visibility.Hidden;
            imgHangman8.Visibility = Visibility.Hidden;
            imgHangman9.Visibility = Visibility.Hidden;
            imgHangman10.Visibility = Visibility.Hidden;




        }
        private void btnVerbergWoord_Click(object sender, RoutedEventArgs e)
        { 
            //teller variable ingeven 
            Countdown();
            //geen geheimwoord ingegeven
            if(txtIngeefveld.Text == "")
            {
                lblOpmerking.Content = "Woord is leeg !!";
                
            }
                       
            else
            {
                //is er een cijfer ingegeven?
                if(countDownIngavenNummer == false)
                {
                    lblOpmerking.Content = "Bedenktijd ongeldig cijfer";
                }
                else
                {
                     //bedenktijd lang genoeg?
                    if(ingegevencountdownNummer < 5 )
                    {
                        lblOpmerking.Content = "Bedenktijd te kort (>5sec.)";
                    }
                    else if (ingegevencountdownNummer>20)
                    {
                        lblOpmerking.Content = "Bedenktijd te lang (<20sec.)";
                    }
                    //spel mag beginnen
                    else
                    {
                        //scherm en knoppen juist zetten
                        geheimWoord = txtIngeefveld.Text;

                        SpelStartRequarments();
                                          

                        //teller starten 
                        teller.Start();
                        countDownteller.Start();

                        //masking en tekst        
                         Masking(); 
                    }
                
                }
                                    
            }                        

        }

        private void btnRaad_Click(object sender, RoutedEventArgs e)
        {
            

            //timer resetten
            countdownnumer = ingegevencountdownNummer;


            //ingegeven tekst als variable zetten
            geraden = txtIngeefveld.Text;

           //word er een woord ingegeven,letter of niks?
                //niks igegeven
            if(geraden.Length==0)
            {
                levens--;

                lblOpmerking.Content = $"Je hebt niks ingegeven";

                TextInfo();

                

                
            }
                //letter ingegeven
            else if (geraden.Length == 1)
            {
                //letters nalezen
                for (int i = 0; i < geheimWoord.Length+1; i++)
                {
                   
                    //letter is juist 
                     if(geheimWoord.Contains(geraden))
                     {
                        letterJuist = true;
                     }
                     //letter is fout
                     else
                     {
                        letterJuist = false;
                     }
                   
                }
                //uitvoering juist letter
                if (letterJuist == true)
                {
                    //per letter 
                    for (int i = 0; i< geheimWoord.Length; i++)
                    {
                        //welke letter is geraden 
                        if (geheimWoord[i] == Convert.ToChar(geraden))
                        {
                            //geraden letter in de array gooien 
                            verborgenLetters[i] = Convert.ToChar(geraden);

                        }
                       
                        
                    }
                    //text variable aanpassen
                    verborgenGeheimWoord = "";
                    for( int j = 0; j < geheimWoord.Length; j++)
                    {                            
                        verborgenGeheimWoord += verborgenLetters[j] + " ";
                    }

                    //alle letters geraden??
                    for (int x = 0; x < geheimWoord.Length; x++)
                    {
                        controleerletterwoord += verborgenLetters[x];
                    }
                    //uitvoering 
                    if (controleerletterwoord == geheimWoord)//alle letters gevonden
                    {
                        Gewonnen();
                        
                    }
                    else//letter is allen juist 
                    {
                        juistenLetters += $" {geraden}";

                        lblOpmerking.Content = $"Letter Juist geraden";
                        TextInfo();

                        txtIngeefveld.Text = "";
                        
                        //anders vult die de foute bij en moet gereset worden
                        controleerletterwoord = "";

                    }
                    


                }
                //foute letter uitvoering
                else
                {
                    fouteLetters += $" {geraden}"; 
                    
                    levens--;

                    lblOpmerking.Content = $"Letter fout geraden";
                    TextInfo();
                    
                    txtIngeefveld.Text = "";

                }               
            }
                //woord ingegeven wanr lenght > 1
            else
            {

                    //woord geraden, gewonnen
                if (geraden == geheimWoord)
                {
                    Gewonnen();                                   
                                        
                }
                    //woord niet geraden
                else if(geraden != geheimWoord)
                {
                    levens--;

                    lblOpmerking.Content = $"Woord fout geraden";
                    TextInfo();                   

                    txtIngeefveld.Text = "";
                }


            }
            //game over verloren 
            if( levens == 0)
            {
                GameOver();
                
                btnScore.Visibility = Visibility.Visible;
            }


            //afbeelding
            AfbeeldingTonen();

            txtIngeefveld.Focus();

           
        }

        private void btn1Speler_Click(object sender, RoutedEventArgs e)
        {
            //computer kiest een geheimwoord

            string[] randomWoorden = {"cavia", "krukje","tijd","fors","sambal","zuivel", "kritisch", "jasje","giga","dieren","lepel","picknick","quasi","verzenden","winnaar","dextrose","vrezen","niqaab","quote","botox", "cruciaal","zitting","cabaret","bewogen","vrijuit","ijverig","cake","dyslexie","uier","sausje","kuuroord","poppetje","docent","camping","schijn","quiz","chef","nacht","cacao","quad","boxer","fysiek","twijg","geloof","vreugde","stuk","volk","stijl","val","joggen","broek","mooi","kwik","werksfeer","dak","klacht","nieuw","vorm","vis" };
           

            Random random = new Random();
            int index = random.Next( randomWoorden.Length); 
            
            //countdown var ingeven
            Countdown();

            //is er een cijfer ingegeven?
            if (countDownIngavenNummer == false)
            {
                lblOpmerking.Content = "Bedenktijd ongeldig cijfer";
            }
            else
            {
                     //bedenktijd lang genoeg?
                    if (ingegevencountdownNummer < 5)
                    {
                        lblOpmerking.Content = "Bedenktijd te kort (>5sec.)";
                    }
                    else if (ingegevencountdownNummer > 20)
                    {
                        lblOpmerking.Content = "Bedenktijd te lang (<20sec.)";
                    }
                    //spel mag beginnen
                    else
                    {
                       //zelfde als verberg woord (2spelers modes)
                        SpelStartRequarments();

                        //random woord pakken
                        geheimWoord = randomWoorden[index];

                        //masking en tekst
                        Masking();

           
                        //teller starten 
                        teller.Start();
                        countDownteller.Start();
                    }    
            }      
        } 
        private void btnHint_Click(object sender, RoutedEventArgs e)
        {
            countdownnumer = ingegevencountdownNummer;
            
            Hint();        
        }
        
        private void btnScore_Click(object sender, RoutedEventArgs e)
        {
            if(scoreOpen == false)
            {
                //bool aanpassen
                scoreOpen = true;
                //score laten zien
                txtScoreBord.Text = "";
                txtScoreBord.Visibility = Visibility.Visible;

                //UI aanpassen
                btnHint.Visibility = Visibility.Hidden;
                btnNieuwSpel.Visibility = Visibility.Hidden;
                btnScore.Content = "Verberg scores";
               /* btnScore.Visibility = Visibility.Hidden;
                btnScoreWeg.Visibility = Visibility.Visible;*/

                       

                //arrays vullen
                spelersNamenArray = spelersNamen.ToArray();
                spelersTijdenArray = spelerTijden.ToArray();
                spelersLevenArray = spelerLeven.ToArray();

                for (int i = 0; i<idx;i++)
                {
                   txtScoreBord.Text += $"{spelersNamenArray[i]} - {spelersLevenArray[i]} levens - ({spelersTijdenArray[i]})\n";
                }
            }
            else
            {
                //bool aanpassen
                scoreOpen = false;

                btnHint.Visibility = Visibility.Visible;
                btnNieuwSpel.Visibility = Visibility.Visible;               
                btnScore.Content = "Toon scores";              
                txtScoreBord.Visibility = Visibility.Hidden;
            }

            
        } 
        
      



        // privite voids-----------------------------------------------------------------------------------------
        private void Masking()
        {
            verborgenLetters = new char[geheimWoord.Length];

            //verborgenletterarray opvullen met char

            for (int i = 0; i < geheimWoord.Length; i++)
            {
                verborgenLetters[i] = '-';

                verborgenGeheimWoord += verborgenLetters[i] + " ";
            }

           

            lblInfo.Content = $"Raad de letters of\n" +
                              $"het een woord\n" +
                              $"je hebt {levens} levens\n" +
                              $"{verborgenGeheimWoord}";
        }

        private void TextInfo()
        {
            //levens en de juiste en foute en het gemask woord weergeven in de info box
            lblInfo.Content =     $"{levens} levens\n" +
                                  $"Juiste letters: {juistenLetters} \n" +
                                  $"Foute letters: {fouteLetters}\n" +
                                  $"{verborgenGeheimWoord}";
        }

        private void AfbeeldingTonen()
        {
            if (levens == 10)
            {
                imgHangman0.Visibility = Visibility.Visible;
            }
            else if (levens == 9)
            {
                imgHangman0.Visibility = Visibility.Hidden;
                imgHangman1.Visibility = Visibility.Visible;
            }
            else if (levens == 8)
            {
                imgHangman1.Visibility = Visibility.Hidden;
                imgHangman2.Visibility = Visibility.Visible;
            }
            else if (levens == 7)
            {
                imgHangman2.Visibility = Visibility.Hidden;
                imgHangman3.Visibility = Visibility.Visible;
            }
            else if (levens == 6)
            {
                imgHangman3.Visibility = Visibility.Hidden;
                imgHangman4.Visibility = Visibility.Visible;
            }
            else if (levens == 5)
            {
                imgHangman4.Visibility = Visibility.Hidden;
                imgHangman5.Visibility = Visibility.Visible;
            }
            else if (levens == 4)
            {
                imgHangman5.Visibility = Visibility.Hidden;
                imgHangman6.Visibility = Visibility.Visible;
            }
            else if (levens == 3)
            {
                imgHangman6.Visibility = Visibility.Hidden;
                imgHangman7.Visibility = Visibility.Visible;
            }
            else if (levens == 2)
            {
                imgHangman7.Visibility = Visibility.Hidden;
                imgHangman8.Visibility = Visibility.Visible;
            }
            else if (levens == 1)
            {
                imgHangman8.Visibility = Visibility.Hidden;
                imgHangman9.Visibility = Visibility.Visible;
            }
            else if (levens == 0)
            {
                imgHangman9.Visibility = Visibility.Hidden;
                imgHangman10.Visibility = Visibility.Visible;
            }
        }

        private void GameOver()
        {
            lblOpmerking.Content = $"Game over !!!";

            lblInfo.Content = $"Woord niet geraden\n" +
                              $"Het woord was {geheimWoord}";

            btnRaad.IsEnabled = false;
            btnHint.Visibility = Visibility.Hidden;

            teller.Stop();
            countDownteller.Stop();
        }

        private void Countdown()
        {               
            
            countDownIngavenNummer = int.TryParse(txtingaveCountdown.Text, out ingegevencountdownNummer);
            if (countDownIngavenNummer == true)
            {
                //een variable maken voor op andere plaatsen te gebruiken 
                ingegevencountdownNummer = Convert.ToInt32(txtingaveCountdown.Text);
                //ingeven in de countdownvar
                countdownnumer = ingegevencountdownNummer;
            }
            
            

        }

        private void scoreBord()
        {
            /*  spelersNamen[idx] = spelerNaam;
              spelerLeven[idx] = levens;
              spelerTijden[idx] = timer.ToLongTimeString();*/
            
            //nulpointerfout
            spelersNamen.Add($"{spelerNaam}");
            spelerLeven.Add(levens);
            spelerTijden.Add($"{timer.ToLongTimeString()}");
                    

            //index van de arrays telkens ene hoger maken 
            idx++;
        }

        private void Hint()
        {
            txtIngeefveld.Focus();

            //Random eruithalen
            Random rand = new Random();
            hintletter = alfabet[rand.Next(alfabet.Length)];
            if(maxHint-1 == 0)
            {
                MessageBox.Show("Max aantal hints berijkt", "max. hint");
            }
            else
            {
                
                //controleren of het niet in het woord zit
                if (!geheimWoord.Contains(hintletter))
                {
                    if (fouteLetters == null)
                    {
                        hintGebruikt = true;

                            MessageBox.Show($"{hintletter} : zit NIET in het woord", "Hint");

                            fouteLetters += $" {hintletter}";
                            TextInfo();
                    }
                    else
                    {
                        if (!fouteLetters.Contains(hintletter))
                        {
                            hintGebruikt = true;

                            MessageBox.Show($"{hintletter} : zit NIET in het woord", "Hint");

                            fouteLetters += $" {hintletter}";
                            TextInfo();

                            maxHint--;

                        }
                        else
                        {
                            Hint(); //letter zit al bij foute letters, nieuwe random genereren
                        }
                    
                    }              
                
                
                }
                else
                {
                    Hint();  //letter zit in het woord, operniuw een random genereren
                }
            }

            
        }

        private void SpelStartRequarments()
        {
            btnVerbergWoord.Visibility = Visibility.Hidden;
            btnRaad.Visibility = Visibility.Visible;
            btnRaad.IsEnabled = true;
            btn1Speler.Visibility = Visibility.Hidden;
            txtIngeefveld.Text = "";
            lblcountdown.Visibility = Visibility.Visible;
            lblcountdownvraag.Visibility = Visibility.Hidden;
            txtingaveCountdown.Visibility = Visibility.Hidden;
            btnHint.Visibility = Visibility.Visible;
            btnScore.Visibility = Visibility.Hidden;
            lblOpmerking.Content = "";

            lblcountdown.Content = ingegevencountdownNummer;

            maxHint = 5;

            txtIngeefveld.Focus();
        }

        private void Gewonnen()
        {
            btnRaad.IsEnabled = false;
            txtIngeefveld.IsEnabled = false;
            btnHint.Visibility = Visibility.Hidden;

            lblOpmerking.Content = $"Proficiat !!!!!!";

            lblInfo.Content = $"Spel gewonnen \n" +
                              $"Het woord was \n" +
                              $"{geheimWoord}";

            txtIngeefveld.Text = "";
            //timers stoppen
            countDownteller.Stop();
            teller.Stop();

            btnScore.IsEnabled = true;

            //score bord als hint niet gebruikt is 
            if (hintGebruikt == false)
            {
                spelerNaam = Interaction.InputBox("Geef je naam in voor het scorebord", "Invoer", "");

                //lblInfo.Content = spelerNaam;
                scoreBord();

                countDownteller.Stop();

                btnScore.Visibility = Visibility.Visible;
            }
            else
            {
                MessageBox.Show("U heeft een hint gebruikt, hierdoor komt u niet op het scorebord", "opmerking");
            }
        }



        //hovereffect( blauwer hover effect in de weg, niet geleerd weg te halen!!! ) ----------------------------------------------------------------------
        //btnRaad
        private void btnRaad_MouseEnter(object sender, MouseEventArgs e)
        {
            btnRaad.BorderBrush = Brushes.Red;
            
        }

        private void btnRaad_MouseLeave(object sender, MouseEventArgs e)
        {
            btnRaad.BorderBrush = Brushes.Gray;
        }
        //btnHint
        private void btnHint_MouseEnter(object sender, MouseEventArgs e)
        {
            btnHint.BorderBrush = Brushes.Red;
        }

        private void btnHint_MouseLeave(object sender, MouseEventArgs e)
        {
            btnHint.BorderBrush = Brushes.Gray;
        }

        private void btn1Speler_MouseEnter(object sender, MouseEventArgs e)
        {
            btn1Speler.BorderBrush = Brushes.Red;
        }

        private void btn1Speler_MouseLeave(object sender, MouseEventArgs e)
        {
            btn1Speler.BorderBrush = Brushes.Gray;
        }

        private void btnNieuwSpel_MouseEnter(object sender, MouseEventArgs e)
        {
            btnNieuwSpel.BorderBrush = Brushes.Red;
        }

        private void btnNieuwSpel_MouseLeave(object sender, MouseEventArgs e)
        {
            btnNieuwSpel.BorderBrush = Brushes.Gray;
        }

        private void btnScore_MouseEnter(object sender, MouseEventArgs e)
        {
            btnScore.BorderBrush = Brushes.Red;
        }

        private void btnScore_MouseLeave(object sender, MouseEventArgs e)
        {
            btnScore.BorderBrush = Brushes.Gray;
        }

        private void btnVerbergWoord_MouseEnter(object sender, MouseEventArgs e)
        {
            btnVerbergWoord.BorderBrush = Brushes.Red;
        }

        private void btnVerbergWoord_MouseLeave(object sender, MouseEventArgs e)
        {
            btnVerbergWoord.BorderBrush = Brushes.Gray;
        }
    }
}
